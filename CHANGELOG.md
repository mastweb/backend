## 1.4.0 (2023-08-13)

### Feat

- add bulk site addition endpoint

### Refactor

- divide misc & add user-public router
- add docstrings to final tests

## 1.3.1 (2023-07-08)

### Fix

- allow api to start without internet connectivity

## 1.3.0 (2023-04-22)

### Feat

- add endpoint to delete current user account

### Fix

- transfer sites to another user on account deletion

## 1.2.3 (2023-04-21)

### Fix

- add external_id field for phaenonet integration

## 1.2.2 (2023-04-04)

### Fix

- bulk importer datetime parser

## 1.2.1 (2023-04-02)

### Fix

- update /data endpoint with additional props

## 1.2.0 (2023-04-02)

### Feat

- add endpoint to export data for a given user

## 1.1.0 (2023-04-02)

### Feat

- endpoint generate presigned s3 url, site img upload
- add boto3 S3 secrets to config, with validation
- add in_stand for site model, subscribed for user model

### Fix

- db init script using python db module
- BUCKET_PATH param double slash on root
- S£_BUCKET_PATH validator returning None
- allow graceful failure of raster data triggers in db
- allow traceparent header in nginx config for otel preflight
- add ping/pong probe endpoint, instead of using /docs

### Refactor

- hardcode boto3 logging to INFO level (too verbose)
- ruff on codebase
- update verse.wsl.ch --> mail.wsl.ch
- dockerfile chown entire appuser dir to prevent permission issues

## 1.0.0 (2022-12-05)

First stable app release.

### Feat

- add systematic and date_obs to data return

## 0.4.2 (2022-12-03)

### Fix

- return value for eastness,northness,slope /data endpoint

## 0.4.1 (2022-11-22)

### Fix

- update gzip encoding to accept additional types
- update ST_Value to ST_NearestValue in db scripts (prevent multiple)

## 0.4.0 (2022-11-22)

### Feat

- update mapdata --> data endpoint, include optional extrametadata
- working db triggers for all vector & raster layer extraction
- db functions and triggers for extrametadata auto-generation

### Refactor

- extrametadata fields to have correct decimal precision
- remove geo api, moved data extraction to postgres db triggers
- update max_charts for extrametadata fields
- update ExtraMetadata fields to nullable and migrate

### Fix

- excel template file (broken from compression/stripping)
- importer error if field is NaN, log message returned in JSONResponse
- add wsl email server to docker-compose aliases
- importer - remove unit from pd_datetime as format already specified

## 0.3.0 (2022-10-04)

### Fix

- remove ... for fastapi File, no longer required
- omit mast_type from Site pydantic models (avoid error where id 0 == None)
- add new field data_source, plus instructions on how to add new field

### Feat

- add support for bbox filter on returning all Sites

### Refactor

- rename root router to misc, add public docstrings

## 0.2.11 (2022-09-01)

### Fix

- /register endpoint to user post data user id instead of Auth0User

## 0.2.10 (2022-08-31)

### Fix

- add verbose debug logging for /register endpoint user

## 0.2.9 (2022-08-31)

### Fix

- replace buggy get_or_create with separate funcs for /register endpoint

## 0.2.8 (2022-08-31)

### Refactor

- rename debug profiles for vscode

### Fix

- update default xlsx_import user to be admin mastweb@protonmail.com

## 0.2.7 (2022-08-25)

### Fix

- update helm chart hostAlias to include smtp server

## 0.2.6 (2022-08-25)

### Fix

- update /register to trigger email and slack webhook, add welcome email

### Refactor

- update user model to make name and nickname fields nullable

## 0.2.5 (2022-08-12)

### Fix

- update UserIn pydantic model to exclude date fields

### Refactor

- subclass AnyUrl to add capacitor to allowed_schemes
- change AnyHttpUrl validator to AnyUrl to allow for capacitor:// origin

## 0.2.4 (2022-08-09)

### Fix

- working import endpoint, add template xlsx endpoint
- allow uploading both csv and xlsx via importer, auto determine charset
- add host alias to db container for local debug name resolution

## 0.2.3 (2022-08-09)

### Fix

- add geojson-pydantic for /mapdata endpoint response model

## 0.2.2 (2022-08-06)

### Fix

- update mapdata endpoint to include additional metadata

### Refactor

- fix passing required fields to Site.only() for mapdata

## 0.2.1 (2022-08-06)

### Fix

- working scripts, importer datetimes, db trigger / function syntax
- import files from directory to recursively import
- move import and mapdata endpoints to root to prevent overlap with site/{id}

### Refactor

- lint db script, strip to essentials
- update models for site and user, Pydantic SiteMap for map data
- importer typing, fix directory glob, await for api bulk import

## 0.2.0 (2022-08-02)

### Feat

- working xlsx import script, hardcoded db values file

### Fix

- update logging date format, add local debug dotenv functions
- create SmallInt subclass override to allow numbers < 1
- debug image not finding files, debugpy as dev dep in pdm

## 0.1.1 (2022-07-14)

### Fix

- update auth0 implementation to include admin scope
- update root paths without trailing slash, add file upload for testing, docstrings
- serve statics, add favicon route, redirect root to docs, docstrings

### Refactor

- minor tweaks to models, add image_url to sites, docstrings
- admin-->editor for get species, remove admin from root page, sort imports

## 0.1.0 (2022-06-05)

### Refactor

- allow species scientific name to be null, re-init migrations
- update migrations to reflect models
- refinement to app models + syntax, add validators

### Feat

- add auth, remove user add endpoint, split routers
- basic crud api for 3 models, test placeholder, http 500 handler
