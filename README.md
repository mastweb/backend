# mastapi

Backend database and API for MastWeb.

**Production Instance**: <a href="https://api.mastweb.ch" target="_blank">https://api.mastweb.ch/</a>

## License

This project is licensed under the terms of the MIT license.

## Usage

### Getting Data

**Manually**

- Visit the URL, e.g. https://api.mastweb.ch/data
- Copy the JSON, or save to a file.
- This is only possible for public endpoints.

**From the Docs Page**

- See the section on `Testing Endpoints` below.
- This may be the easiest approach if the endpoint requires authorisation.
- However, the Docs UI can't support huge amounts of data, so endpoints which return 'all' may fail.

**From Software**

- Software such as Postman can be used to access APIs.
  https://www.postman.com
- It can also handle an authorisation header.

**From Code**

- You can get the data from any program script.
- For example in Python, you would uses requests.get to return data.
- Data can be parsed from JSON to a dictionary and used from there.
- You must pass the authorization header in format:
  `Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9`
- Example:

```python
import requests

# Get data
r = requests.get('https://api.mastweb.ch/site', headers={'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'})
```

**From the Database**

- It is possible to directly access the database.
- No headers are required, only usename and password.
- SQL can be used to directly query tables.
- Be **very** careful using this option. Only use if all else fails.

> Note: you must connect using a PostgreSQL connection, with the
> correct host, database name, user, and password.

**From the Database - QGIS**

- Load a recent version of QGIS and add a connection under PostgreSQL.
- Enter database credentials to connect.
- Load the `site` table: points and attribute table can be viewed.

**Getting Access Tokens Manually**

- Sign into auth0.com dashboard to access the Client Secret for MastAPI.
- From terminal / cURL:

```bash
curl --request POST \
  --url 'https://dev-0p1cmjh6.eu.auth0.com/oauth/token' \
  --header 'content-type: application/x-www-form-urlencoded' \
  --data 'client_id=jhFtA7Q77cBu4MmC6F3czF8RLRKPcSvT' \
  --data 'client_secret=YOUR_CLIENT_SECRET_HERE' \
  --data 'audience=mastapi.envidat.ch' \
  --data 'grant_type=client_credentials'
```

- From Python:

```python
import http.client

conn = http.client.HTTPSConnection("dev-0p1cmjh6.eu.auth0.com")

payload = "{\"client_id\":\"jhFtA7Q77cBu4MmC6F3czF8RLRKPcSvT\",\"client_secret\":\"YOUR_CLIENT_SECRET_HERE\",\"audience\":\"mastapi.envidat.ch\",\"grant_type\":\"client_credentials\"}"

headers = { 'content-type': "application/json" }

conn.request("POST", "/oauth/token", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
```

### Testing Endpoints

- The docs page also doubles as a testing page.

- **Public** endpoints can be used by default:

  - Expand the selection to view details.
  - Click `Try it out`.
  - If the endpoint only returns data, `Execute` directly.
  - If the endpoint requires data, add a valid JSON to the box first.

- **Authenticated** endpoints require login:

  - Click the `Authorize` button at the top right.
  - In the `Auth0ImplicitBearer` section, enter client_id:
    `jhFtA7Q77cBu4MmC6F3czF8RLRKPcSvT`
    - Tick the 'admin' box if you wish to use admin privileges.
  - Login as normal.
  - You should return to the docs page, where you can use authenticated endpoints.

- Endpoint types:
  - GET: read from the database.
  - PUT: update an existing record.
  - POST: add a new record.
  - DELETE: delete a record.

### Bulk Import Data

- See README within `scripts` directory.

### Adding New Fields

- See README within `app/models` directory.

## Development

### Create Secrets

- Create a directory in the root of the repo called `secret`.
- Create the following files, inside the `secret` directory:

  - **db.env**

  ```dotenv
  DB_HOST=xxx
  DB_NAME=xxx
  DB_USER=xxx
  DB_PASS=xxx
  ```

  - **debug.env**

  ```dotenv
  # App
  SECRET_KEY=xxx
  BACKEND_CORS_ORIGINS='["http://localhost:8000","http://172.0.0.1:8000","https://xxx.xxx.ch"]'
  # Auth0
  AUTH0_DOMAIN=domain.auth0.com
  AUTH0_API_AUDIENCE=audience_string
  # Email
  SMTP_SERVER=smtp-server.com
  SMTP_MAIL_FROM=admin@domain.com
  SMTP_CC=data-manager@domain.com
  SLACK_WEBHOOK=https://hooks.slack.com/services/some_webhook
  # S3
  S3_ENDPOINT=https://s3.aws.com
  S3_REGION=region_name
  S3_ACCESS_KEY=xxxx
  S3_SECRET_KEY=xxxx
  S3_BUCKET_NAME=mastweb
  S3_BUCKET_PATH=data/user_images/
  ```

  > NOTE: S3_BUCKET_PATH is optional and will default to the bucket root.

### Run Server

- With development database:

```bash
docker compose up -d
**connect IDE to start debugger and run app**
```

- OR without development database:

```bash
docker compose up -d api
**connect IDE to start debugger and run app**
```

- The configuration for connecting to the app for VSCode is included in this repo.
  - Go to the debug menu.
  - Select `Remote - Server Debug`.

## Production

### Docker

- The Gitlab CI/CD pipeline automatically deploys via Docker.
- Variables required in Gitlab:
  - RUNTIME_ENV containing all variables for the app.
  - DB_ENV containing database connection credentials.

### Kubernetes

To deploy via Kubernetes, see the helm chart README, in the `chart` directory.
