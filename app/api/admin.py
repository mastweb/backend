"""Admin API router."""

import logging
from typing import Union

from fastapi import APIRouter

from app.auth import get_admin

log = logging.getLogger(__name__)

router = APIRouter(
    prefix="/admin",
    tags=["admin"],
)


@router.put("/loglevel/{log_level}")
def change_log_level(
    log_level: Union[str, int],
    user=get_admin(),
):
    """Dynamically change log level when required."""
    logging.getLogger().setLevel(log_level)
    log.info(f"Changing log level to {log_level} via endpoint /loglevel/#.")
    return {"message": f"Log level set to {log_level}"}
