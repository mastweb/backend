"""Misc API Router."""

import logging
from io import BytesIO
from pathlib import Path
from uuid import uuid4

from boto3 import client as s3client
from charset_normalizer import detect as detect_charset
from fastapi import APIRouter, UploadFile
from fastapi.encoders import jsonable_encoder
from fastapi.responses import (
    FileResponse,
    JSONResponse,
    RedirectResponse,
)
from geojson_pydantic import Feature
from pydantic.error_wrappers import ValidationError

from app.auth import get_editor
from app.config import settings
from app.models.site import Site, SitePydanticMap
from scripts.importer import import_mast_data

log = logging.getLogger(__name__)
logging.getLogger("boto3").setLevel(logging.INFO)
logging.getLogger("botocore").setLevel(logging.INFO)

router = APIRouter(
    tags=["misc"],
)


@router.get("/", include_in_schema=False)
async def home():
    """Redirect home to docs."""
    return RedirectResponse("/docs")


@router.get("/favicon.ico", include_in_schema=False)
async def favicon():
    """Serve static favicon."""
    return RedirectResponse("/static/favicon.ico")


@router.get("/ping", include_in_schema=False)
async def pong():
    """Check that the API is operational. Used by K8S probes."""
    return JSONResponse(status_code=200, content={"ping": "pong!"})


@router.get("/template")
async def download_template_spreadsheet():
    """Download a template xlsx for the data import endpoint."""
    return FileResponse(
        "./app/static/template.xlsx",
        filename="mastweb_template.xlsx",
        media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    )


@router.post("/import")
async def bulk_upload_from_spreadsheet(file: UploadFile, user=get_editor()):
    """Bulk upload site data from Excel spreadsheet."""
    log.debug(f"Reading data from file: {file}")
    contents = await file.read()
    char_encoding = detect_charset(contents).get("encoding")
    log.debug(f"Encoding determined as {char_encoding}")
    log.debug("Opening data buffer")
    buffer = BytesIO(contents)

    try:
        extension = Path(file.filename).suffix
        log.info(f"Attempting import for uploaded {extension} file")
        await import_mast_data(
            buffer,
            user_id=user.id,
            file_extension=extension,
            char_encoding=char_encoding,
        )
    except KeyError as e:
        log.error("Import of data file failed")
        return JSONResponse(status_code=422, content={"message": f"{e}"})
    except ValidationError as e:
        log.error("Validation error during bulk import")
        log.error(e)
        msg = str(e).split("ValidationWrap")[1:]
        return JSONResponse(status_code=422, content={"message": msg})
    except Exception as e:
        log.error("Import of data file failed")
        log.error(e)
        return JSONResponse(status_code=500, content={"message": "Failed bulk import"})
    finally:
        log.debug("Closing data buffer")
        buffer.close()

    return JSONResponse(status_code=200, content={"message": "Successful bulk import"})


@router.get("/data", response_model=list[Feature])
async def get_sites_data_for_map(minimal: bool = False):
    """Get site data for map and table display.

    WGS84 coordinates are swapped to [lon,lat] for GeoJSON format.

    THIS IS A PUBLIC ENDPOINT.

    Args:
        minimal (bool): Return without extra metadata/properties.
    """
    required_fields = [
        "id",
        "species_id",
        "mast_type_id",
        "mast_year",
        "geohash",
        "date_observation",
        "systematic",
        "image_url",
        "data_source",
        "in_stand",
    ]
    log.debug(f"Getting site fields: {required_fields}")
    map_sites = await SitePydanticMap.from_queryset(Site.all().only(*required_fields))

    if minimal:
        return JSONResponse(
            status_code=200,
            content=[
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [
                            float(site.coord_wgs84[1]),
                            float(site.coord_wgs84[0]),
                        ],
                    },
                    "id": str(site.id),
                    "properties": {
                        "mast_type_id": site.mast_type_id,
                        "mast_year": site.mast_year,
                        "species_id": site.species_id,
                    },
                }
                for site in map_sites
            ],
        )

    return JSONResponse(
        status_code=200,
        content=[
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [
                        float(site.coord_wgs84[1]),
                        float(site.coord_wgs84[0]),
                    ],
                },
                "id": str(site.id),
                "properties": {
                    "mast_type_id": site.mast_type_id,
                    "mast_year": site.mast_year,
                    "species_id": site.species_id,
                    "date_observation": jsonable_encoder(site.date_observation),
                    "systematic": site.systematic,
                    "image_url": site.image_url,
                    "data_source": site.data_source,
                    "in_stand": site.in_stand,
                    "canton": site.extra.canton if site.extra else None,
                    "municipality": site.extra.municipality if site.extra else None,
                    "biogeographic_region": site.extra.biogeographic_region
                    if site.extra
                    else None,
                    "vegetation_belt": site.extra.vegetation_belt
                    if site.extra
                    else None,
                    "elevation": site.extra.elevation if site.extra else None,
                    "slope": jsonable_encoder(site.extra.slope) if site.extra else None,
                    "eastness": jsonable_encoder(site.extra.eastness)
                    if site.extra
                    else None,
                    "northness": jsonable_encoder(site.extra.northness)
                    if site.extra
                    else None,
                },
            }
            for site in map_sites
        ],
    )


@router.get("/site-image-upload-url")
async def get_site_image_upload_url(user=get_editor()):
    """Generate a pre-signed URL to upload a site image to S3.

    The S3 path is generated with the S3_BUCKET_PATH parameter, plus
    a randomly generated uuid and .jpg suffix.

    Uploaded files must be in JPEG format.
    """
    client = s3client(
        "s3",
        aws_access_key_id=settings.S3_ACCESS_KEY,
        aws_secret_access_key=settings.S3_SECRET_KEY,
        endpoint_url=settings.S3_ENDPOINT,
        region_name=settings.S3_REGION,
    )

    try:
        if settings.S3_BUCKET_PATH:
            key = f"{settings.S3_BUCKET_PATH}{uuid4()}.jpg"
        else:
            key = f"{uuid4()}.jpg"

        signed_url = client.generate_presigned_url(
            ClientMethod="put_object",
            Params={
                "Bucket": settings.S3_BUCKET_NAME,
                "Key": key,
            },
            ExpiresIn=3600,
        )
        return JSONResponse(status_code=200, content={"url": signed_url})

    except Exception as e:
        log.error(e)
        return JSONResponse(
            status_code=500, content={"message": f"Error uploading file: {e}"}
        )
