"""Root router to import all other routers."""

import logging
from typing import Callable

from fastapi import APIRouter, Depends, HTTPException, Request, Response
from fastapi.routing import APIRoute

from app.api import admin, misc, site, species, user, user_public
from app.auth import auth

log = logging.getLogger(__name__)


class RouteErrorHandler(APIRoute):
    """Custom APIRoute that handles application errors and exceptions."""

    def get_route_handler(self) -> Callable:
        """Original route handler for exetension."""
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            """Route handler with improved logging."""
            try:
                return await original_route_handler(request)
            except Exception as e:
                if isinstance(e, HTTPException):
                    raise Exception from e
                log.exception(f"Uncaught error: {e}")

                raise HTTPException from e(status_code=500, detail=str(e))

        return custom_route_handler


api_router = (
    APIRouter(dependencies=[Depends(auth.implicit_scheme)]) if auth else APIRouter()
)
api_router.include_router(misc.router)
api_router.include_router(admin.router)
api_router.include_router(user.router)
api_router.include_router(user_public.router)
api_router.include_router(species.router)
api_router.include_router(site.router)
api_router.include_router(site.bulk_router)

error_router = APIRouter(route_class=RouteErrorHandler)
