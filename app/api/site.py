"""Site API Router."""

import logging
from typing import Optional

from fastapi import APIRouter, HTTPException, Query
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from tortoise.contrib.fastapi import HTTPNotFoundError

from app.auth import get_editor
from app.models.site import (
    Site,
    SiteEditPydantic,
    SiteInPydantic,
    SitePydantic,
    SitePydanticExtra,
)

log = logging.getLogger(__name__)


router = APIRouter(
    prefix="/site",
    tags=["site"],
)
bulk_router = APIRouter(
    tags=["site"],
)


class Status(BaseModel):
    """Helper class to display message from API after function run."""

    message: str


@router.get("", response_model=list[SitePydantic])
async def get_sites(
    bbox: Optional[list[str]] = Query(
        default=None,
        description="x_min,y_min,x_max,y_max",
        example="7.079315,46.650379,8.180695,47.192512",
    ),
    user=get_editor(),
):
    """Get all sites, or filtered subset.

    Args:
        bbox (list[float]): List of decimal WGS84 coordinates forming a box.
            Used to filter sites within specified box.
            Format: x_min, y_min, x_max, y_max.
            Example: 7.079315,46.650379,8.180695,47.192512.
        user (Auth0User): Logged in user. Automatically provided.
    """
    if bbox:
        log.debug(f"Splitting bbox string: {bbox}")
        try:
            x_min, y_min, x_max, y_max = bbox.pop().split(",")
        except ValueError:
            log.error(f"Invalid bbox string passed as parameter for get_sites: {bbox}")
            return JSONResponse(
                status_code=422,
                content={"msg": "Not a valid bbox. Must be x_min,y_min,x_max,y_max"},
            )
        log.debug(
            "Filtering sites by x_min, y_min, x_max, y_max: "
            f"{x_min, y_min, x_max, y_max}"
        )
        sql = f"""
            SELECT *
            FROM site
            WHERE site.geometry @ ST_MakeEnvelope (
                {x_min}, {y_min}, {x_max}, {y_max}, 4326
            );
        """
        site_ids = [str(site.id) for site in await Site.raw(sql)]
        log.debug(f"Site IDs within bbox: {site_ids}")
        return await SitePydantic.from_queryset(Site.filter(mast_type_id__in=[1]))

    log.debug("Getting all sites")
    return await SitePydantic.from_queryset(Site.all())


@router.post("", response_model=SiteInPydantic)
async def create_site(site: SiteInPydantic, user=get_editor()):
    """Create new site."""
    log.debug(f"Creating new site with params: {site}")
    site_obj = await Site.create(**site.dict(exclude_unset=True))
    log.info(f"Created new site with ID {site_obj.id}")
    return await SiteInPydantic.from_tortoise_orm(site_obj)


@router.get(
    "/{id}",
    response_model=SitePydanticExtra,
    responses={404: {"model": HTTPNotFoundError}},
)
async def get_site(id: str, user=get_editor()):
    """Get specific site."""
    log.debug(f"Getting Site ID {id}")
    return await SitePydanticExtra.from_queryset_single(Site.get(id=id))


@router.put(
    "/{id}",
    response_model=SiteEditPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def update_site(id: str, site: SiteEditPydantic, user=get_editor()):
    """Update specific site."""
    log.debug(f"Attempting to update Site ID {id} with params: {site}")
    await Site.filter(id=id).update(**site.dict(exclude_unset=True))
    log.info(f"Site ID {id} updated.")
    return await SiteEditPydantic.from_queryset_single(Site.get(id=id))


@router.delete(
    "/{id}", response_model=Status, responses={404: {"model": HTTPNotFoundError}}
)
async def delete_site(id: str, user=get_editor()):
    """Delete specific site."""
    log.debug(f"Attempting to delete Site ID {id}")
    deleted_count = await Site.filter(id=id).delete()
    if not deleted_count:
        log.error(f"Failed deleting site ID {id}. Does not exist ")
        raise HTTPException(status_code=404, detail=f"Site {id} not found")
    log.info(f"Deleted Site ID {id}")
    return Status(message=f"Deleted site {id}")


@bulk_router.post("/site-bulk", response_model=list[SiteInPydantic])
async def create_site_bulk(sites: list[SiteInPydantic], user=get_editor()):
    """Create new sites in bulk."""
    log.debug(f"Bulk creating new sites with params: {sites}")
    site_objs = await Site.bulk_create(
        [Site(**site.dict(exclude_unset=True)) for site in sites]
    )

    log.info(f"Bulk created {len(site_objs)} sites")

    site_return = [
        await SiteInPydantic.from_tortoise_orm(site_obj) for site_obj in site_objs
    ]
    return site_return
