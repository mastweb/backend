"""Species API Router."""

import logging

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
from tortoise.contrib.fastapi import HTTPNotFoundError

from app.auth import get_admin, get_editor
from app.models.species import (
    Species,
    SpeciesEditPydantic,
    SpeciesInPydantic,
    SpeciesPydantic,
)

log = logging.getLogger(__name__)

router = APIRouter(
    prefix="/species",
    tags=["species"],
)


class Status(BaseModel):
    """Helper class to display message from API after function run."""

    message: str


@router.get("", response_model=list[SpeciesPydantic])
async def get_all_species(user=get_editor()):
    """Get all species."""
    log.debug("Getting all species")
    return await SpeciesPydantic.from_queryset(Species.all())


@router.post("", response_model=SpeciesInPydantic)
async def create_species(species: SpeciesInPydantic, user=get_admin()):
    """Create new species."""
    log.debug(f"Creating new species with params: {species}")
    species_obj = await Species.create(**species.dict(exclude_unset=True))
    return await SpeciesInPydantic.from_tortoise_orm(species_obj)


@router.get(
    "/{id}",
    response_model=SpeciesPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def get_species(id: int, user=get_editor()):
    """Get specific species."""
    log.debug(f"Getting Species ID {id}")
    return await SpeciesPydantic.from_queryset_single(Species.get(id=id))


@router.put(
    "/{id}",
    response_model=SpeciesEditPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def update_species(id: int, species: SpeciesEditPydantic, user=get_admin()):
    """Update specific species."""
    log.debug(f"Attempting to update Species ID {id} with params: {species}")
    await Species.filter(id=id).update(**species.dict(exclude_unset=True))
    return await SpeciesEditPydantic.from_queryset_single(Species.get(id=id))


@router.delete(
    "/{id}", response_model=Status, responses={404: {"model": HTTPNotFoundError}}
)
async def delete_species(id: int, user=get_admin()):
    """Delete specific species."""
    log.debug(f"Attempting to delete Species ID {id}")
    deleted_count = await Species.filter(id=id).delete()
    if not deleted_count:
        log.error(f"Failed deleting species ID {id}. Does not exist ")
        raise HTTPException(status_code=404, detail=f"Species {id} not found")
    return Status(message=f"Deleted species {id}")
