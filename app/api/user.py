"""User API Router."""

import logging

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
from tortoise.contrib.fastapi import HTTPNotFoundError

from app.auth import get_admin
from app.models.site import Site
from app.models.user import UserAccount, UserAccountEditPydantic, UserAccountPydantic

log = logging.getLogger(__name__)

router = APIRouter(
    prefix="/user",
    tags=["user"],
)


class Status(BaseModel):
    """Helper class to display message from API after function run."""

    message: str


@router.get("", response_model=list[UserAccountPydantic])
async def get_users(user=get_admin()):
    """Get all users."""
    return await UserAccountPydantic.from_queryset(UserAccount.all())


@router.get(
    "/{sub}",
    response_model=UserAccountPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def get_user(sub: str, user=get_admin()):
    """Get specific user."""
    return await UserAccountPydantic.from_queryset_single(UserAccount.get(sub=sub))


@router.put(
    "/{sub}",
    response_model=UserAccountEditPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def update_user(
    sub: str, user_pydantic: UserAccountEditPydantic, user=get_admin()
):
    """Update specific user."""
    await UserAccount.filter(sub=sub).update(**user_pydantic.dict(exclude_unset=True))
    return await UserAccountEditPydantic.from_queryset_single(UserAccount.get(sub=sub))


@router.delete(
    "/{sub}", response_model=Status, responses={404: {"model": HTTPNotFoundError}}
)
async def delete_user(sub: str, user=get_admin()):
    """Delete specific user.

    Will also transfer ownership of any sites to the current user.
    """
    log.debug(f"Getting sites associated with user {sub}")
    sites = await Site.filter(user_id=sub)

    if sites:
        log.info(f"Found {len(sites)} sites for user {sub}")
        log.info(f"Transfering ownership of sites to user {user.id}")
        current_uid = await UserAccount.get(sub=user.id).values_list("sub", flat=True)
        await Site.filter(user_id=sub).update(user_id=current_uid)
    else:
        log.info(f"No sites found associated with user {sub}")

    log.debug(f"Attempting deletion of user {sub}")
    deleted_count = await UserAccount.filter(sub=sub).delete()
    if not deleted_count:
        log.error(f"Failed deleting user {sub}. Does not exist ")
        raise HTTPException(status_code=404, detail=f"User {sub} not found")
    return Status(message=f"Deleted user {sub}")
