"""User Public API Router."""

import logging
from email.mime.text import MIMEText

from aiosmtplib import send as send_email
from fastapi import APIRouter, BackgroundTasks, HTTPException
from fastapi.responses import (
    StreamingResponse,
)
from jinja2 import Template
from pydantic import BaseModel
from slack_sdk.webhook import WebhookClient
from tortoise.contrib.fastapi import HTTPNotFoundError

from app.auth import get_editor
from app.config import settings
from app.models.site import Site, SitePydanticExport
from app.models.user import (
    UserAccount,
    UserAccountEditPydantic,
    UserAccountInPydantic,
    UserAccountPydantic,
)
from scripts.db_options import mast_options, species_options
from scripts.importer import convert_dict_to_csv_stream

log = logging.getLogger(__name__)

router = APIRouter(
    tags=["user"],
)


class Status(BaseModel):
    """Helper class to display message from API after function run."""

    message: str


@router.get(
    "/me",
    response_model=UserAccountPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def get_current_user(user=get_editor()):
    """Details for currently logged in user."""
    log.debug(f"Getting user with ID: {user.id}")
    return await UserAccountPydantic.from_queryset_single(UserAccount.get(sub=user.id))


@router.put(
    "/me",
    response_model=UserAccountEditPydantic,
    responses={404: {"model": HTTPNotFoundError}},
)
async def update_current_user(
    user_pydantic: UserAccountEditPydantic, user=get_editor()
):
    """Update logged in user details."""
    log.debug(f"Updating user with ID: {user.id}")
    await UserAccount.filter(sub=user.id).update(
        **user_pydantic.dict(exclude_unset=True)
    )
    return await UserAccountEditPydantic.from_queryset_single(
        UserAccount.get(sub=user.id)
    )


@router.delete(
    "/me",
    response_model=Status,
    responses={404: {"model": HTTPNotFoundError}},
)
async def delete_current_user(
    user=get_editor(),
):
    """Delete logged in user account.

    <b>WARNING</b>: Use with care!

    Will also transfer ownership of any sites to an admin user.
    """
    log.debug(f"Getting sites associated with user {user.id}")
    sites = await Site.filter(user_id=user.id)

    if sites:
        log.info(f"Found {len(sites)} sites for user {user.id} during deletion")
        log.debug("Getting admin user account from database")
        admin_uid = await UserAccount.first().values_list("sub", flat=True)
        log.info(f"Transfering ownership of sites to user {admin_uid}")
        await Site.filter(user_id=user.id).update(user_id=admin_uid)
    else:
        log.debug(f"No sites found associated with user {user.id}")

    log.debug(f"Attempting deletion of user {user.id}")
    deleted_count = await UserAccount.filter(sub=user.id).delete()
    if not deleted_count:
        log.error(f"Failed deleting user {user.id}. Does not exist in database?")
        raise HTTPException(status_code=404, detail=f"User deletion failed {user.id}")
    return Status(message=f"Deleted user {user.id}")


@router.get(
    "/get-my-data",
)
async def get_all_data_submitted_by_user(user=get_editor()):
    """For a given user, download all of the data they have submitted as CSV."""
    log.info(f"Getting data for user ID: {user.id}")
    site_list = await SitePydanticExport.from_queryset(Site.filter(user_id=user.id))

    log.debug("Formatting data before CSV insert")
    site_list = [site.dict() for site in site_list]

    log.debug("Updating fields mast_type_id and species_id to descriptive names")
    mast_map = {v: k for k, v in mast_options.items()}
    species_map = {v: k for k, v in species_options.items()}
    for _idx, site in enumerate(site_list):
        site["mast_type"] = mast_map[site["mast_type_id"]]
        del site["mast_type_id"]
        site["species"] = species_map[site["species_id"]]
        del site["species_id"]

    log.debug(f"Converted sites: {site_list}")
    stream_csv = convert_dict_to_csv_stream(site_list)

    response = StreamingResponse(iter([stream_csv.getvalue()]), media_type="text/csv")
    response.headers["Content-Disposition"] = "attachment; filename=export.csv"

    return response


@router.post("/register", response_model=UserAccountInPydantic)
async def register_user(
    new_user: UserAccountInPydantic,
    background_tasks: BackgroundTasks,
    user=get_editor(),
):
    """Register new user to database. Used by Auth0 post-login action."""

    def render_jinja_template(template_path, **context):
        """Render a jinja2 html file."""
        with open(template_path) as file:
            template = Template(file.read())
        return template.render(context)

    async def send_welcome_email(user_name: str, user_email: str):
        """Send welcome email to newly registered user."""
        message = MIMEText(
            render_jinja_template(
                "./app/email_templates/welcome_email.html",
                user_name=user_name,
                user_email=user_email,
            ),
            "html",
        )
        message["From"] = settings.SMTP_MAIL_FROM
        message["Cc"] = settings.SMTP_CC
        message["To"] = user_email
        message["Subject"] = "Welcome to MastWeb"
        log.debug("Sending email to new user")
        await send_email(message, hostname=settings.SMTP_SERVER, port=25)

    def trigger_slack_webhook(user_email: str):
        """Notify slack channel that new user logged in."""
        webhook = WebhookClient(settings.SLACK_WEBHOOK)
        log.debug("Creating slack notification for new user")
        webhook.send(text=f"New user: {user_email}")

    log.debug("Checking if user exists")
    log.debug(f"Auth details: {user}")
    log.debug(f"Post data: {new_user}")
    user_obj = await UserAccount.get_or_none(sub=new_user.sub)

    if not user_obj:
        log.debug(f"Adding new user to database {new_user}")
        user_obj = await UserAccount.create(**new_user.dict(exclude_unset=True))
        log.info(f"New user registered: {user_obj.email}")

        background_tasks.add_task(send_welcome_email, user_obj.nickname, user_obj.email)

        if settings.SLACK_WEBHOOK:
            background_tasks.add_task(trigger_slack_webhook, user_obj.email)

    return await UserAccountInPydantic.from_tortoise_orm(user_obj)
