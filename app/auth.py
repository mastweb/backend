"""Implementation of Auth0 auth."""

import logging

from fastapi import Security
from fastapi_auth0 import Auth0, Auth0User

from app.config import settings

log = logging.getLogger(__name__)


try:
    auth = Auth0(
        domain=settings.AUTH0_DOMAIN,
        api_audience=settings.AUTH0_API_AUDIENCE,
        scopes={"admin": "Admin priviledge"},
    )
except Exception:
    log.warning("Auth0 failed to initialise. Are you running offline?")
    log.warning("The app will continue without auth functionality.")
    auth = {}


def get_editor() -> Auth0User:
    """Determine if user is logged in, and hence can edit."""
    return Security(auth.get_user) if auth else None


def get_admin() -> Auth0User:
    """Determine if user has admin rights. Set per individual in Auth0."""
    return Security(auth.get_user, scopes=["admin"]) if auth else None
