"""Config file for Pydantic and FastAPI, using environment variables."""

from typing import Any, Optional, Union

from pydantic import AnyUrl, BaseSettings, PostgresDsn, validator


class AnyHttpUrlPlusCapacitor(AnyUrl):
    """Override AnyUrl to all capacitor as a scheme option."""

    allowed_schemes = {"http", "https", "capacitor"}


class Settings(BaseSettings):
    """Main settings class, defining environment variables."""

    APP_NAME: str
    SECRET_KEY: str
    DEBUG: str = False
    LOG_LEVEL: str = "INFO"

    SMTP_SERVER: str
    SMTP_MAIL_FROM: str
    SMTP_CC: Optional[str] = None

    SLACK_WEBHOOK: Optional[str] = None

    BACKEND_CORS_ORIGINS: list[AnyHttpUrlPlusCapacitor] = []

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, list[str]]) -> Union[list[str], str]:
        """Build and validate CORS origins list."""
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    DB_HOST: str
    DB_USER: str
    DB_PASS: str
    DB_NAME: str
    DB_URI: Optional[PostgresDsn] = None

    @validator("DB_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: dict[str, Any]) -> Any:
        """Build Postgres connection from environment variables."""
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgres",
            user=values.get("DB_USER"),
            password=values.get("DB_PASS"),
            host=values.get("DB_HOST"),
            path=f"/{values.get('DB_NAME') or ''}",
        )

    AUTH0_DOMAIN: str
    AUTH0_API_AUDIENCE: str

    S3_ENDPOINT: Optional[str]
    S3_REGION: Optional[str]
    S3_ACCESS_KEY: Optional[str]
    S3_SECRET_KEY: Optional[str]
    S3_BUCKET_NAME: Optional[str]
    S3_BUCKET_PATH: Optional[str]

    @validator("S3_BUCKET_PATH", pre=True)
    def check_all_s3_settings_present(cls, v: Optional[str], values: dict[str, Any]):
        """Validate that all S3 parameters exist, if one is present."""
        if any(
            [
                values.get("S3_ENDPOINT"),
                values.get("S3_REGION"),
                values.get("S3_ACCESS_KEY"),
                values.get("S3_SECRET_KEY"),
                values.get("S3_BUCKET_NAME"),
            ]
        ):
            if None in (
                values.get("S3_ENDPOINT"),
                values.get("S3_REGION"),
                values.get("S3_ACCESS_KEY"),
                values.get("S3_SECRET_KEY"),
                values.get("S3_BUCKET_NAME"),
            ):
                err_str = (
                    "If an S3 config param is provided, then all must be present: "
                    "S3_ENDPOINT, S3_REGION, S3_ACCESS_KEY, "
                    "S3_SECRET_KEY, S3_BUCKET_NAME"
                )
                raise ValueError(err_str)

        if v is None:
            # Set bucket path to "/"" if none provided
            return "/"
        return v

    class Config:
        """Pydantic settings config."""

        case_sensitive = True


settings = Settings()
