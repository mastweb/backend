# Models

- Models use Tortoise-ORM to connect to the database:
  https://github.com/tortoise/tortoise-orm
- Docs: https://tortoise.github.io/

## Adding Fields

**Add to the Python model file**

- Edit the file, e.g. `site.py`.
- Add the field under the model class, e.g. `Site`.
- Field types and parameters can be found at: https://tortoise.github.io/fields.html
  - Examples include:
    - `CharField` for text.
    - `IntField` for integer numbers.
    - `DatetimeField` for date fields.

**Run the development server**

- See README in the root of the repo.
- Once connected the development server will be running, including your model edits.

**Connect to the development server**

- From a terminal, run: `docker exec -it mastapi`

**Run migrations**

- From within the development container:

```bash
aerich --app mastapi migrate
aerich --app mastapi upgrade
```

- The database should be updated.

**Commit the migrations**

- To keep a record of the database update, from a terminal in the repo directory, run:

```bash
git add .
git commit -m "feat: add new field to database"
git push
```
