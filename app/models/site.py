"""Models associated with masting site records."""

import logging

import geohash2
from tortoise import Tortoise, fields, models
from tortoise.contrib.pydantic import pydantic_model_creator

from app.config import settings
from app.db import TORTOISE_ORM
from app.models.validators import (
    DatetimeValidator,
    FutureYearValidator,
    GeohashValidator,
)

log = logging.getLogger(__name__)


class PKSmallIntField(fields.SmallIntField):
    """Override class for SmallIntField constraints.

    Allows for values of 0 when pk=True
    By default generated=True if pk=True, despite manually specifying.
    """

    @property
    def constraints(self) -> dict:
        """Constraints for Pydantic validation."""
        return {
            "ge": -1,
            # "le": 18446744073709551615,
            "le": 3,
        }


class MastType(models.Model):
    """The MastType model.

    Fixed mast event types.
    0 = Fehlmast
    1 = Sprengmast
    2 = Halbmast
    3 = Vollmast
    """

    id = PKSmallIntField(pk=True, generated=False)
    name = fields.CharField(max_length=10)

    def __str__(self):
        """Return the mast type name by default."""
        return str(self.name)

    class Meta:
        """Tortoise config."""

        app = settings.APP_NAME


class Site(models.Model):
    """The Site model.

    For storing masting events.
    """

    id = fields.UUIDField(pk=True)
    species = fields.ForeignKeyField(
        "mastapi.Species", related_name="species", on_delete="RESTRICT"
    )
    mast_type = fields.ForeignKeyField(
        "mastapi.MastType",
        related_name="mast_type",
        on_delete="RESTRICT",
        generated=False,
    )
    mast_year = fields.SmallIntField(validators=[FutureYearValidator()])
    # Geohash precision 10 = (1.2m x 59.5cm accuracy at equator)
    # This is equivalent to 6 decimal places in WGS84 standard coordinates
    geohash = fields.CharField(max_length=10, validators=[GeohashValidator()])
    observer = fields.CharField(null=True, max_length=50)
    user = fields.ForeignKeyField(
        "mastapi.UserAccount", related_name="user", on_delete="RESTRICT"
    )
    systematic = fields.BooleanField(default=False)
    date_observation = fields.DatetimeField(null=True, validators=[DatetimeValidator()])
    date_entry = fields.DatetimeField(auto_now_add=True)
    date_modified = fields.DatetimeField(auto_now=True)
    comments = fields.CharField(null=True, max_length=256)
    image_url = fields.CharField(null=True, max_length=512)
    data_source = fields.CharField(default="mastapi", max_length=128)
    in_stand = fields.BooleanField(default=True)
    external_id = fields.CharField(null=True, max_length=256)

    def __str__(self):
        """Return the site id as a string."""
        return str(self.id)

    def coord_wgs84(self) -> list:
        """WGS84 coordinate from geohash, format [lat,lon]."""
        lat, lon = geohash2.decode(self.geohash)
        return [lat, lon]

    class Meta:
        """Tortoise config."""

        app = settings.APP_NAME
        ordering = ["date_observation", "date_entry"]

    class PydanticMeta:
        """Pydantic config."""

        computed = ["coord_wgs84"]
        exclude = [
            "species.genus",
            "species.family",
            "species.name_english",
            "species.name_german",
            "species.name_french",
            "species.name_italian",
            "species.priority",
            "user.auth_type",
            "user.name",
            "user.nickname",
            "user.email",
            "user.given_name",
            "user.family_name",
            "user.picture",
            "user.locale",
            "user.subscribed",
            "user.updated_at",
            "user.date_added",
        ]


class ExtraMetadata(models.Model):
    """The ExtraMetadata model.

    For storing associated metadata with sites.
    """

    site = fields.OneToOneField("mastapi.Site", related_name="extra", pk=True)
    elevation = fields.SmallIntField(null=True, max_digits=4)
    slope = fields.DecimalField(null=True, max_digits=5, decimal_places=2)
    eastness = fields.DecimalField(null=True, max_digits=5, decimal_places=2)
    northness = fields.DecimalField(null=True, max_digits=5, decimal_places=2)
    canton = fields.CharField(null=True, max_length=30)
    municipality = fields.CharField(null=True, max_length=50)
    biogeographic_region = fields.CharField(null=True, max_length=50)
    vegetation_belt = fields.CharField(null=True, max_length=50)

    def __str__(self):
        """Return site id string as default."""
        return str(self.id)

    class Meta:
        """Tortoise config."""

        app = settings.APP_NAME


# We must init models prior to Pydantic serialisation, else no foreign keys
Tortoise.init_models(
    TORTOISE_ORM["apps"][settings.APP_NAME]["models"], settings.APP_NAME
)

SitePydantic = pydantic_model_creator(Site, name="Site", exclude=["extra", "mast_type"])
SiteInPydantic = pydantic_model_creator(
    Site,
    name="SiteIn",
    exclude_readonly=True,
)
SiteEditPydantic = pydantic_model_creator(
    Site,
    name="SiteEdit",
    exclude_readonly=True,
    optional=[
        "species_id",
        "mast_type_id",
        "mast_year",
        "geohash",
        "user_id",
        "observer",
        "date_observation",
        "systematic",
        "comments",
        "image_url",
        "data_source",
        "in_stand",
        "external_id",
    ],
)
SitePydanticExtra = pydantic_model_creator(
    Site, name="SiteExtra", exclude=["mast_type"]
)
SitePydanticMap = pydantic_model_creator(
    Site,
    name="SiteMap",
    include=[
        "id",
        "species_id",
        "mast_type_id",
        "mast_year",
        "coord_wgs84",
        "date_observation",
        "systematic",
        "image_url",
        "data_source",
        "in_stand",
        "extra",
    ],
)
SitePydanticExport = pydantic_model_creator(
    Site,
    name="SiteExport",
    include=[
        "id",
        "species_id",
        "mast_type_id",
        "mast_year",
        "coord_wgs84",
        "date_observation",
        "date_entry",
        "systematic",
        "image_url",
        "in_stand",
        "comments",
    ],
)
ExtraMetadataPydantic = pydantic_model_creator(ExtraMetadata, name="ExtraMetadata")
ExtraMetadataInPydantic = pydantic_model_creator(
    ExtraMetadata, name="ExtraMetadataIn", exclude_readonly=True
)
