"""Models associated with species records."""

from tortoise import fields, models
from tortoise.contrib.pydantic import pydantic_model_creator

from app.config import settings
from app.models.validators import EmptyStringValidator


class Species(models.Model):
    """The Species model.

    Fixed species types.
    """

    id = fields.SmallIntField(pk=True, generated=True)
    scientific_name = fields.CharField(null=True, max_length=100, unique=True)
    genus = fields.CharField(max_length=100, validators=[EmptyStringValidator()])
    family = fields.CharField(max_length=100, validators=[EmptyStringValidator()])
    name_english = fields.CharField(max_length=100, null=True)
    name_german = fields.CharField(max_length=100, null=True)
    name_french = fields.CharField(max_length=100, null=True)
    name_italian = fields.CharField(max_length=100, null=True)
    priority = fields.BooleanField(default=False)

    def __str__(self):
        """Return the scientific name by default."""
        return self.scientific_name

    class Meta:
        """Tortoise config."""

        app = settings.APP_NAME
        ordering = ["-priority", "scientific_name"]

    class PydanticMeta:
        """Pydantic config."""

        exclude = ["priority"]


SpeciesPydantic = pydantic_model_creator(Species, name="Species")
SpeciesInPydantic = pydantic_model_creator(
    Species,
    name="SpeciesIn",
    exclude_readonly=True,
)
SpeciesEditPydantic = pydantic_model_creator(
    Species,
    name="SpeciesEdit",
    exclude_readonly=True,
    optional=["scientific_name", "genus", "family"],
)
