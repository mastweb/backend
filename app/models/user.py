"""Models associated with user records."""

from tortoise import fields, models
from tortoise.contrib.pydantic import pydantic_model_creator

from app.config import settings
from app.models.validators import EmptyStringValidator


class UserAccount(models.Model):
    """Model to record login from various provider accounts."""

    sub = fields.CharField(pk=True, generated=False, max_length=255)
    auth_type = fields.CharField(
        null=False, max_length=50, validators=[EmptyStringValidator()]
    )
    email = fields.CharField(
        null=False, max_length=255, validators=[EmptyStringValidator()]
    )
    name = fields.CharField(null=True, max_length=255)
    nickname = fields.CharField(null=True, max_length=255)
    given_name = fields.CharField(null=True, max_length=255)
    family_name = fields.CharField(null=True, max_length=255)
    picture = fields.CharField(null=True, max_length=255)
    locale = fields.CharField(null=True, max_length=2)
    subscribed = fields.BooleanField(default=False)
    updated_at = fields.DatetimeField(auto_now=True)
    date_added = fields.DatetimeField(auto_now_add=True)

    class Meta:
        """Tortoise config."""

        app = settings.APP_NAME

    class PydanticMeta:
        """Pydantic config."""

        computed = ["display_name"]
        exclude = ["display_name"]

    def __str__(self):
        """Return the display_name by default."""
        return self.display_name()

    def display_name(self) -> str:
        """Determine the display name."""
        if self.given_name or self.family_name:
            return f"{self.given_name or ''} {self.family_name or ''}".strip()
        elif self.nickname:
            return self.nickname
        return self.email


class PydanticMetaOverride:
    """Alternative class for PydanticMeta.

    Required due to bug in Tortoise pydantic_model_creator 14-07-2022.
    To use the 'sub' primary key field on user record creation.
    """

    computed = []


UserAccountPydantic = pydantic_model_creator(UserAccount, name="User")
UserAccountInPydantic = pydantic_model_creator(
    UserAccount,
    name="UserIn",
    meta_override=PydanticMetaOverride,
    exclude=["updated_at", "date_added"],
)
UserAccountEditPydantic = pydantic_model_creator(
    UserAccount,
    name="UserEdit",
    exclude_readonly=True,
    optional=["auth_type", "name", "nickname", "email", "subscribed"],
)
