"""Custom validators for model fields."""

import logging
from datetime import date, datetime

import geohash2
from tortoise.exceptions import ValidationError
from tortoise.validators import Validator

log = logging.getLogger(__name__)


class EmptyStringValidator(Validator):
    """Validate whether a string is empty."""

    def __call__(self, value: str):
        """Check value."""
        if not value:
            raise ValidationError("Value can not be empty")


class FutureYearValidator(Validator):
    """Validate if an integer year is not in the future."""

    def __call__(self, year: int):
        """Check value."""
        current_year = date.today().year
        if year > current_year:
            log.error("Mast year is invalid. Must be in the past")
            raise ValidationError("Mast year is invalid. Must be in the past")


class DatetimeValidator(Validator):
    """Validate if datetime is in correct format and falls within specified range."""

    def __call__(self, entered_date: str):
        """Check value."""
        current_year = datetime.now().year
        if entered_date.year > current_year:
            log.error("Input date invalid. Must be in the past")
            raise ValidationError("Input date invalid. Must be in the past")

        if entered_date.year < date.fromisoformat("1990-01-01").year:
            log.error("Input date invalid. Cannot be before 1990")
            raise ValidationError("Input date invalid. Cannot be before 1990")


class GeohashValidator(Validator):
    """Validate a geohash as a geometry."""

    def __call__(self, geohash: str):
        """Check value."""
        try:
            lat, lon = geohash2.decode(geohash)
        except Exception as e:
            log.error(e)
            log.error(f"Failed to decode geohash {geohash}")
            raise ValidationError from e("Not a valid geohash. Failed to decode")
