# MastWeb API Chart

Chart for MastAPI Backend in FastAPI.

- Interfaces with production database.
- Can autoscale with load.

## Secrets

Requires secrets to be pre-populated.

- **mastapi-vars** prod db password, fastapi variables

  - key: DB_PASS  (for production database)
  - key: SECRET_KEY  (for fastapi)
  - key: BACKEND_CORS_ORIGINS  (list in format '["http://...", "..."]')
    - Note: also add the Auth0 domain to the CORS list.

  ```bash
  kubectl create secret generic mastapi-vars \
  --from-literal=DB_PASS=xxxxxxx \
  --from-literal=SECRET_KEY=xxxxxxx \
  --from-literal=BACKEND_CORS_ORIGINS='["xxx", "xxx"]'
  ```

- **oauth-vars** oauth credentials for Auth0

  - key: AUTH0_DOMAIN
  - key: AUTH0_API_AUDIENCE

  ```bash
  kubectl create secret generic oauth-vars \
    --from-literal=AUTH0_DOMAIN=xxxxxxx \
    --from-literal=AUTH0_API_AUDIENCE=xxxxxxx
  ```

- **smtp-vars** SMTP credentials for email

  - key: SMTP_SERVER
  - key: SMTP_MAIL_FROM
  - key: SMTP_CC (optional)
  - key: SLACK_WEBHOOK (optional)

  ```bash
  kubectl create secret generic smtp-vars \
    --from-literal=SMTP_SERVER=xxxxxxx \
    --from-literal=SMTP_MAIL_FROM=xxxxxxx \
    --from-literal=SMTP_CC=xxxxxxx \
    --from-literal=SLACK_WEBHOOK=xxxxxxx
  ```

- **envidat-star** for https / tls certs

  - Standard Kubernetes TLS secret for \*.envidat.ch

## Deployment

```shell
helm upgrade --install mastapi oci://registry-gitlab.wsl.ch/mastweb/backend --namespace mastweb --create-namespace
```
