-- upgrade --
CREATE TABLE IF NOT EXISTS "useraccount" (
    "sub" VARCHAR(255) NOT NULL  PRIMARY KEY,
    "auth_type" VARCHAR(50) NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "nickname" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "given_name" VARCHAR(255),
    "family_name" VARCHAR(255),
    "picture" VARCHAR(255),
    "locale" VARCHAR(2),
    "updated_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "date_added" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
COMMENT ON TABLE "useraccount" IS 'Model to record login from various provider accounts.';
CREATE TABLE IF NOT EXISTS "species" (
    "id" SMALLSERIAL NOT NULL PRIMARY KEY,
    "scientific_name" VARCHAR(100)  UNIQUE,
    "genus" VARCHAR(100) NOT NULL,
    "family" VARCHAR(100) NOT NULL,
    "name_english" VARCHAR(100),
    "name_german" VARCHAR(100),
    "name_french" VARCHAR(100),
    "name_italian" VARCHAR(100),
    "priority" BOOL NOT NULL  DEFAULT False
);
COMMENT ON TABLE "species" IS 'The Species model.';
CREATE TABLE IF NOT EXISTS "masttype" (
    "id" SMALLINT NOT NULL  PRIMARY KEY,
    "name" VARCHAR(10) NOT NULL
);
COMMENT ON TABLE "masttype" IS 'The MastType model.';
CREATE TABLE IF NOT EXISTS "site" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "mast_year" SMALLINT NOT NULL,
    "geohash" VARCHAR(10) NOT NULL,
    "observer" VARCHAR(50),
    "systematic" BOOL NOT NULL  DEFAULT False,
    "date_observation" TIMESTAMPTZ,
    "date_entry" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "date_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "comments" VARCHAR(256),
    "image_url" VARCHAR(512),
    "mast_type_id" SMALLINT NOT NULL REFERENCES "masttype" ("id") ON DELETE RESTRICT,
    "species_id" SMALLINT NOT NULL REFERENCES "species" ("id") ON DELETE RESTRICT,
    "user_id" VARCHAR(255) NOT NULL REFERENCES "useraccount" ("sub") ON DELETE RESTRICT
);
COMMENT ON TABLE "site" IS 'The Site model.';
CREATE TABLE IF NOT EXISTS "extrametadata" (
    "elevation" SMALLINT NOT NULL,
    "slope" DECIMAL(5,2) NOT NULL,
    "eastness" DECIMAL(5,2) NOT NULL,
    "northness" DECIMAL(5,2) NOT NULL,
    "canton" VARCHAR(30) NOT NULL,
    "municipality" VARCHAR(30) NOT NULL,
    "biogeographic_region" VARCHAR(30) NOT NULL,
    "vegetation_belt" VARCHAR(30) NOT NULL,
    "site_id" UUID NOT NULL  PRIMARY KEY REFERENCES "site" ("id") ON DELETE CASCADE
);
COMMENT ON TABLE "extrametadata" IS 'The ExtraMetadata model.';
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(100) NOT NULL,
    "content" JSONB NOT NULL
);
