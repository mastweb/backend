-- upgrade --
ALTER TABLE "useraccount" ALTER COLUMN "name" DROP NOT NULL;
ALTER TABLE "useraccount" ALTER COLUMN "nickname" DROP NOT NULL;
ALTER TABLE "site" ADD "data_source" VARCHAR(128)   DEFAULT 'mastapi';
ALTER TABLE "site" ALTER COLUMN "mast_type_id" TYPE SMALLINT USING "mast_type_id"::SMALLINT;
-- downgrade --
ALTER TABLE "site" DROP COLUMN "data_source";
ALTER TABLE "site" ALTER COLUMN "mast_type_id" TYPE SMALLINT USING "mast_type_id"::SMALLINT;
ALTER TABLE "useraccount" ALTER COLUMN "name" SET NOT NULL;
ALTER TABLE "useraccount" ALTER COLUMN "nickname" SET NOT NULL;
