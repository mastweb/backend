from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "extrametadata" ALTER COLUMN "biogeographic_region" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "vegetation_belt" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "canton" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "eastness" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "eastness" TYPE DECIMAL(3,2) USING "eastness"::DECIMAL(3,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "slope" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "slope" TYPE DECIMAL(3,2) USING "slope"::DECIMAL(3,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "municipality" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "municipality" TYPE VARCHAR(50) USING "municipality"::VARCHAR(50);
        ALTER TABLE "extrametadata" ALTER COLUMN "elevation" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "northness" DROP NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "northness" TYPE DECIMAL(3,2) USING "northness"::DECIMAL(3,2);"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "extrametadata" ALTER COLUMN "biogeographic_region" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "vegetation_belt" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "canton" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "eastness" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "eastness" TYPE DECIMAL(5,2) USING "eastness"::DECIMAL(5,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "slope" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "slope" TYPE DECIMAL(5,2) USING "slope"::DECIMAL(5,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "municipality" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "municipality" TYPE VARCHAR(30) USING "municipality"::VARCHAR(30);
        ALTER TABLE "extrametadata" ALTER COLUMN "elevation" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "northness" SET NOT NULL;
        ALTER TABLE "extrametadata" ALTER COLUMN "northness" TYPE DECIMAL(5,2) USING "northness"::DECIMAL(5,2);"""
