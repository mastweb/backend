from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "extrametadata" ALTER COLUMN "biogeographic_region" TYPE VARCHAR(50) USING "biogeographic_region"::VARCHAR(50);
        ALTER TABLE "extrametadata" ALTER COLUMN "vegetation_belt" TYPE VARCHAR(50) USING "vegetation_belt"::VARCHAR(50);"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "extrametadata" ALTER COLUMN "biogeographic_region" TYPE VARCHAR(30) USING "biogeographic_region"::VARCHAR(30);
        ALTER TABLE "extrametadata" ALTER COLUMN "vegetation_belt" TYPE VARCHAR(30) USING "vegetation_belt"::VARCHAR(30);"""
