from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "extrametadata" ALTER COLUMN "eastness" TYPE DECIMAL(5,2) USING "eastness"::DECIMAL(5,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "slope" TYPE DECIMAL(5,2) USING "slope"::DECIMAL(5,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "northness" TYPE DECIMAL(5,2) USING "northness"::DECIMAL(5,2);"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "extrametadata" ALTER COLUMN "eastness" TYPE DECIMAL(3,2) USING "eastness"::DECIMAL(3,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "slope" TYPE DECIMAL(3,2) USING "slope"::DECIMAL(3,2);
        ALTER TABLE "extrametadata" ALTER COLUMN "northness" TYPE DECIMAL(3,2) USING "northness"::DECIMAL(3,2);"""
