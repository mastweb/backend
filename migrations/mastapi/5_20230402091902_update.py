from tortoise import BaseDBAsyncClient


async def upgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "useraccount" ADD "subscribed" BOOL NOT NULL  DEFAULT False;
        ALTER TABLE "site" ADD "in_stand" BOOL NOT NULL  DEFAULT True;
        ALTER TABLE "site" ALTER COLUMN "data_source" SET NOT NULL;"""


async def downgrade(db: BaseDBAsyncClient) -> str:
    return """
        ALTER TABLE "site" DROP COLUMN "in_stand";
        ALTER TABLE "site" ALTER COLUMN "data_source" DROP NOT NULL;
        ALTER TABLE "useraccount" DROP COLUMN "subscribed";"""
