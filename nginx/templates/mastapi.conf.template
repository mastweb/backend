server {

    listen 80 default_server;
    server_name _;

    client_max_body_size 50M;

    add_header 'Content-Security-Policy' $upgrade_policy;
    add_header 'Access-Control-Allow-Credentials' 'true';
    # For opentelemetry
    add_header 'Access-Control-Allow-Headers' 'traceparent,tracestate';

    location / {

        default_type   text/html;
        error_page     404 = @app;

        try_files $uri @app;
    }

    location @app {
        proxy_pass http://${MASTAPI_HOST};

        proxy_set_header Host                $http_host;
        proxy_set_header X-Real-IP           $remote_addr;
        proxy_set_header X-Forwarded-Proto   $scheme;
        proxy_set_header X-Forwarded-For     $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host    $http_host;
        proxy_set_header X-Forwarded-Server  $http_host;
        proxy_set_header X-Forwarded-Port    $server_port;

    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}