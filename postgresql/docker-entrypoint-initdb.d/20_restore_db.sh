#!/bin/bash
set -e

echo "Backup and restoring $DB_NAME via pipe to new host."
PGPASSWORD="$DB_PASS" pg_dump --verbose --format c \
    --host "$DB_HOST" --username "$DB_USER" "$DB_NAME" \
    | pg_restore --verbose --exit-on-error \
    --username postgres --dbname "$DB_NAME"
