# scripts

Helper scripts to get MastAPI started.

## Import Data Into Database

- Import data from an Excel spreadsheet (.xlsx).
- Mandatory header fields:

  - WSG84_lon (coordinate)
  - WSG84_lat (coordinate)
  - genus (text)
  - mast_name (text)
  - mast_year (number)
  - observer (text)

- Optional header fields:

  - species (text)
  - date_observation (date: DD/MM/YYYY or DD.MM.YYYY)
  - systematic (1/0 or TRUE/FALSE, empty cell defaults to FALSE)
  - comments (text)
  - image_url (text)

- Notes:

  - Species should be their scientific name.
  - Ideally species should be included, along with genus.
  - If the species is undetermined, then it is extacted from genus.
    E.g. Albies sp.
  - If uploading CSV, the charset should be UTF-8 for best results.

### Import via Web UI

- This should be the first approach.
  - https://app.mastweb.ch

### Import programatically

- If you understand software and APIs.
  - Connect ot https://api.mastweb.ch to view endpoint documentation
  - POST the JSON data via the endpoint, with an authentication header.
  - To do this directly in the documentation page first you must be authorised. See the README in the root of this repo.

### Running the script manually

- To import data run the script from any shell (linux:bash, windows:powershell):
  - Place your Excel spreadsheet in scripts/data
  - Run the init script:
    `. init_mastweb.sh`
  - This will do the following:
    - Start the development FastAPI server.
    - Copy the spreadsheet to the server container.
    - Extract data from the spreadsheet and import into the database.
