"""Database initalisation script.

Requirements:
 - Extensions postgis and postgis_raster are enabled.
 - Tables with imported vector boundaries:
    - cantons
    - municipalities
    - biogeographic_regions
    - vegetation_belts
"""

import logging

# from tortoise.connection import ConnectionHandler
from tortoise import Tortoise, run_async
from tortoise.backends.base.client import BaseDBAsyncClient
from tortoise.exceptions import OperationalError

from scripts.utils import load_dotenv_if_in_debug_mode, terminal_log

load_dotenv_if_in_debug_mode(env_file="./secret/debug.env")

from app.db import TORTOISE_ORM  # noqa: E402

log = (
    terminal_log(__name__)
    if not logging.getLogger().hasHandlers()
    else logging.getLogger(__name__)
)


async def db_init(tortoise_config: dict):
    """Init ORM models with Tortoise."""
    log.info("Initialising database.")
    await Tortoise.init(
        config=tortoise_config,
    )


async def execute_sql(conn: BaseDBAsyncClient, sql: str):
    """Execute raw SQL with logging and exception handling."""
    try:
        log.debug(f"Executing SQL: {sql}")
        await conn.execute_script(sql)
        log.debug("Executed SQL successfuly")
    except OperationalError as e:
        log.error(f"SQL execution failed: {sql}")
        log.error(e)


async def enable_gdal_drivers():
    """Enable all GDAL drivers and outdb_rasters."""
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")
    sql = """
        SET postgis.gdal_enabled_drivers = 'ENABLE_ALL';
        SET postgis.enable_outdb_rasters = true;
    """
    log.info(
        "Setting postgis.gdal_enabled_drivers=ENABLE_ALL "
        "and postgis.enable_outdb_rasters = true."
    )
    await execute_sql(conn, sql)
    # conn.close_all()


async def add_postgis_geometry_column(table_name: str, col_name: str):
    """Add a new column to existing postgres table, using raw SQL."""
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")
    sql = f"""
        ALTER TABLE {table_name}
        ADD COLUMN IF NOT EXISTS {col_name}
        geometry(Geometry,4326)
        DEFAULT NULL
        ;
    """
    log.info(f"Creating column {col_name} column in table {table_name}.")
    await execute_sql(conn, sql)
    # conn.close_all()


async def add_postgis_geohash_convert_trigger(
    table_name: str,
    geohash_col_name: str,
    geometry_col_name: str,
):
    """Add a trigger to generate PostGIS geometries from inserted GeoHash."""
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")

    sql = f"""
        CREATE OR REPLACE FUNCTION create_geom_from_geohash()
        RETURNS TRIGGER AS
            $$
            BEGIN
                NEW.{geometry_col_name} =
                ST_SetSRID(
                    ST_PointFromGeoHash(NEW.{geohash_col_name},10)
                    ,4326
                );
            RETURN NEW;
            END;
            $$
        LANGUAGE 'plpgsql'
        ;
    """
    log.info("Creating function create_geom_from_geohash() in DB.")
    await execute_sql(conn, sql)

    sql = f"""
        DROP TRIGGER IF EXISTS trigger_postgis_geoms on public.{table_name};
        CREATE TRIGGER trigger_postgis_geoms
        BEFORE INSERT OR UPDATE
        ON public.{table_name}
        FOR EACH ROW
        EXECUTE PROCEDURE create_geom_from_geohash()
        ;
    """
    log.info("Creating trigger trigger_postgis_geoms() in DB.")
    await execute_sql(conn, sql)
    # conn.close_all()


async def create_init_extra_metadata_function():
    """Add a trigger to init the extra metadata table with an row for a given site.

    Returns:
    -------
        str: Name of init function in DB.
    """
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")

    sql = """
        CREATE OR REPLACE FUNCTION
            create_extrametadata_entry(newsite public.site)
        RETURNS VOID AS
            $$
            BEGIN
                INSERT INTO public.extrametadata (site_id)
                VALUES (newsite.id)
                ON CONFLICT DO NOTHING;
            END;
            $$
        LANGUAGE 'plpgsql'
        ;
    """
    log.info("Creating function create_extrametadata_entry() in DB.")
    await execute_sql(conn, sql)
    return "create_extrametadata_entry"


async def create_vector_layer_functions(
    geometry_col_name: str,
    table_names: list[str],
    from_col_names: list[str],
    to_col_names: str,
):
    """Create functions to extract the value from polygon layers for a given point.

    Args:
        geometry_col_name (str): The name of the geometry column for both tables.
        table_names (list[str]): Name of the tables containing polygon
            geometries.
        from_col_names (list[str]): The name of the column in the polygon
            tables to extract the value from.
        to_col_names (list[str]): The name of the columns in the table to
            insert the value into.

    Returns:
    -------
        list[str]: Name of vector extraction functions in DB.

    Notes:
    -----
        Variables table_names, from_col_names, to_col_names must be of the same
        length and in matching order.
    """
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")

    # Add all functions
    sql = """
        CREATE OR REPLACE FUNCTION
            get_value_for_point_{to_col_name}(
                newsite public.site
            )
        RETURNS VOID AS
            $$
            BEGIN
                UPDATE public.extrametadata
                SET {to_col_name}=subquery.{from_col_name}
                FROM (
                    SELECT poly.{from_col_name}
                    FROM public.{table_name} poly
                    WHERE poly.{geometry_col_name} && newsite.{geometry_col_name}
                ) subquery
                WHERE public.extrametadata.site_id=newsite.id
                ;
            END;
            $$
        LANGUAGE 'plpgsql'
        ;
    """

    for i, _table in enumerate(table_names):
        log.info(f"Creating get_value_for_point_{to_col_names[i]}() in DB.")
        sql_formatted = sql.format(
            geometry_col_name=geometry_col_name,
            table_name=table_names[i],
            from_col_name=from_col_names[i],
            to_col_name=to_col_names[i],
        )
        await execute_sql(conn, sql_formatted)

    # conn.close_all()
    return [f"get_value_for_point_{field}" for field in to_col_names]


async def create_raster_layer_functions(
    geometry_col_name: str,
    col_names: list[str],
):
    """Create functions to extract the value from raster layers for a given point.

    Args:
        geometry_col_name (str): The name of the geometry column for both tables.
        col_names (list[str]): Name of the columns to be extracted, with corresponding
            imported raster table r_dem_<col_name>.

    Returns:
    -------
        list[str]: Name of raster extraction functions in DB.

    Notes:
    -----
        Raster layers must be registered in db using the postgres_raster extension.
        The actual raster layer data can be out-of-db (e.g. COG).
    """
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")

    if "elevation" in col_names:
        col_name = "elevation"
        sql = f"""
            CREATE OR REPLACE FUNCTION
                get_value_for_point_{col_name}(
                    newsite public.site
                )
            RETURNS VOID AS
                $$
                BEGIN
                    UPDATE
                    public.extrametadata
                    SET
                    elevation = (
                    SELECT * from CAST((
                        SELECT
                        MAX(ST_NearestValue(rast, 1, newsite.{geometry_col_name}))
                        FROM
                        public.r_dem_{col_name} rast
                        WHERE
                        ST_ConvexHull(rast) && newsite.{geometry_col_name}
                    ) AS int2)
                    )
                    WHERE
                    public.extrametadata.site_id = newsite.id
                    ;
                EXCEPTION WHEN OTHERS THEN
                    RAISE INFO 'Elevation could not be extracted';
                END;
                $$
            LANGUAGE 'plpgsql'
            ;
        """
        log.info(f"Creating get_value_for_point_{col_name}() in DB.")
        await execute_sql(conn, sql)

    if "slope" in col_names:
        col_name = "slope"
        sql = f"""
            CREATE OR REPLACE FUNCTION
                get_value_for_point_{col_name}(
                    newsite public.site
                )
            RETURNS VOID AS
                $$
                BEGIN
                    UPDATE
                    public.extrametadata
                    SET
                    slope = (
                    SELECT * from ROUND(CAST((
                        SELECT
                        MAX(ST_NearestValue(rast, 1, newsite.{geometry_col_name}))
                        FROM
                        public.r_dem_{col_name} rast
                        WHERE
                        ST_ConvexHull(rast) && newsite.{geometry_col_name}
                    ) AS numeric), 2)
                    )
                    WHERE
                    public.extrametadata.site_id = newsite.id
                    ;
                EXCEPTION WHEN OTHERS THEN
                    RAISE INFO 'Elevation could not be extracted';
                END;
                $$
            LANGUAGE 'plpgsql'
            ;
        """
        log.info(f"Creating get_value_for_point_{col_name}() in DB.")
        await execute_sql(conn, sql)

    if "aspect" in col_names:
        col_name = "aspect"
        sql = f"""
            CREATE OR REPLACE FUNCTION
                get_value_for_point_{col_name}(
                    newsite public.site
                )
            RETURNS VOID AS
                $$
                BEGIN
                    WITH aspect as (
                        SELECT
                        MAX(ST_NearestValue(rast, 1, newsite.{geometry_col_name}))
                        FROM
                        public.r_dem_{col_name} rast
                        WHERE
                        ST_ConvexHull(rast) && newsite.{geometry_col_name}
                    ), eastness_val AS (
                        SELECT
                        sin(aspect.max * pi() / 180)
                        from aspect
                    ), northness_val AS (
                        SELECT
                        cos(aspect.max * pi() / 180)
                        from aspect
                    )
                    UPDATE
                    public.extrametadata
                    SET
                    eastness = (
                    SELECT * from ROUND(CAST((
                        SELECT *
                        FROM eastness_val
                    ) AS numeric), 2)
                    ),
                    northness = (
                    SELECT * from ROUND(CAST((
                        SELECT *
                        FROM northness_val
                    ) AS numeric), 2)
                    )
                    WHERE
                    public.extrametadata.site_id = newsite.id
                    ;
                EXCEPTION WHEN OTHERS THEN
                    RAISE INFO 'Elevation could not be extracted';
                END;
                $$
            LANGUAGE 'plpgsql'
            ;
        """
        log.info(f"Creating get_value_for_point_{col_name}() in DB.")
        await execute_sql(conn, sql)

    # conn.close_all()
    return [f"get_value_for_point_{field}" for field in col_names]


async def add_extra_metadata_vector_triggers(
    func_names: list[str],
):
    """Wrap vector layer extraction functions into a single trigger.

    Args:
        func_names (list[str]): Name of functions to include in trigger.
    """
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")

    # Wrap in trigger functions in parent function
    all_functions = [f"PERFORM {func_name}(NEW);" for func_name in func_names]
    all_functions = "\n".join(all_functions)
    sql = f"""
        CREATE OR REPLACE FUNCTION create_extra_vector_metadata(
        ) RETURNS TRIGGER AS
            $$
            BEGIN
                {all_functions}
            RETURN NEW;
            END;
            $$
        LANGUAGE plpgsql;
    """
    log.info("Creating wrapper function create_extra_vector_metadata() in DB.")
    await execute_sql(conn, sql)

    sql = """
        DROP TRIGGER IF EXISTS
        trigger_extra_vector_metadata ON public.site;
        CREATE TRIGGER trigger_extra_vector_metadata
        AFTER INSERT OR UPDATE
        ON public.site
        FOR EACH ROW
        EXECUTE PROCEDURE create_extra_vector_metadata()
        ;
    """
    log.info("Creating trigger trigger_extra_vector_metadata() in DB.")
    await execute_sql(conn, sql)
    # conn.close_all()


async def add_extra_metadata_raster_triggers(
    func_names: list[str],
):
    """Wrap raster layer extraction functions into a single trigger.

    Args:
        func_names (list[str]): Name of functions to include in trigger.
    """
    # conn = ConnectionHandler.get("default")
    log.debug("Creating new db connection via TortoiseORM.")
    conn = Tortoise.get_connection("default")

    # Wrap in trigger functions in parent function
    all_functions = [f"PERFORM {func_name}(NEW);" for func_name in func_names]
    all_functions = "\n".join(all_functions)
    sql = f"""
        CREATE OR REPLACE FUNCTION create_extra_raster_metadata(
        ) RETURNS TRIGGER AS
            $$
            BEGIN
                {all_functions}
            RETURN NEW;
            END;
            $$
        LANGUAGE plpgsql;
    """
    log.info("Creating wrapper function create_extra_raster_metadata() in DB.")
    await execute_sql(conn, sql)

    sql = """
        DROP TRIGGER IF EXISTS
        trigger_extra_raster_metadata ON public.site;
        CREATE TRIGGER trigger_extra_raster_metadata
        AFTER INSERT OR UPDATE
        ON public.site
        FOR EACH ROW
        EXECUTE PROCEDURE create_extra_raster_metadata()
        ;
    """
    log.info("Creating trigger trigger_extra_raster_metadata() in DB.")
    await execute_sql(conn, sql)
    # conn.close_all()


async def main():
    """Run database init."""
    await db_init(TORTOISE_ORM)

    log.info("START adding database functions and triggers")
    await enable_gdal_drivers()
    # Geometries
    await add_postgis_geometry_column("site", "geometry")
    await add_postgis_geohash_convert_trigger("site", "geohash", "geometry")

    # Vector layer extraction
    init_func_name = await create_init_extra_metadata_function()
    vector_func_names = await create_vector_layer_functions(
        "geometry",
        [
            "v_cantons",
            "v_municipalities",
            "v_biogeographic_regions",
            "v_vegetation_belts",
        ],
        ["name", "name", "deregionna", "hs_en"],
        ["canton", "municipality", "biogeographic_region", "vegetation_belt"],
    )
    # Include init function, as trigger order is random

    # Raster layer extraction
    await add_extra_metadata_vector_triggers([init_func_name] + vector_func_names)
    raster_func_names = await create_raster_layer_functions(
        "geometry",
        [
            "elevation",
            "slope",
            "aspect",
        ],
    )
    # Include init function, as trigger order is random
    await add_extra_metadata_raster_triggers([init_func_name] + raster_func_names)

    log.info("FINISH adding database functions and triggers")


if __name__ == "__main__":
    run_async(main())
