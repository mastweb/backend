"""Import spreadsheet into database, from command line or API endpoint."""

import logging
from difflib import get_close_matches
from io import BytesIO, StringIO
from pathlib import Path
from typing import NoReturn, Union

import geohash2
import pandas as pd
from pydantic import BaseModel
from pydantic.main import ModelMetaclass
from tortoise import run_async

from scripts.db_options import genus_options, mast_options, species_options
from scripts.utils import load_dotenv_if_in_debug_mode, terminal_log

load_dotenv_if_in_debug_mode(env_file="./secret/debug.env")

from app.db import TORTOISE_ORM  # noqa: E402
from app.models.site import Site, SiteInPydantic  # noqa: E402
from scripts.db import db_init  # noqa: E402

log = (
    terminal_log(__name__)
    if not logging.getLogger().hasHandlers()
    else logging.getLogger(__name__)
)

pd.options.mode.chained_assignment = None
current_working_dir = Path.cwd()


def validate_data_schema(data_schema: ModelMetaclass):
    """Validate a pandas.DataFrame against the given data_schema via decorator."""

    def inner(func):
        def wrapper(*args, **kwargs):
            res = func(*args, **kwargs)
            if isinstance(res, pd.DataFrame):
                # check result of the function execution against the data_schema
                df_dict = res.to_dict(orient="records")

                # Wrap the data_schema into a helper class for validation
                class ValidationWrap(BaseModel):
                    df_dict: list[data_schema]

                # Do the validation
                _ = ValidationWrap(df_dict=df_dict)
            else:
                raise TypeError(
                    "Your Function is not returning an object of type pandas.DataFrame."
                )

            # return the function result
            return res

        return wrapper

    return inner


def convert_dict_to_csv_stream(data_dict: dict):
    """Given a data dictionary, convert to CSV using pandas.

    Args:
        data_dict (dict): Dictionary of data columns.

    Returns:
        stream (StirngIO): Data stream StringIO object.
    """
    log.debug("Creating dataframe from data_dict")
    df = pd.DataFrame.from_dict(data_dict)

    stream = StringIO()
    log.debug("Converting dataframe to CSV stream")
    df.to_csv(stream, index=False)
    return stream


def geohash_encode(lat: float, lon: float) -> pd.DataFrame:
    """Create a geohash from WGS84 coordinates, precision 10."""
    log.debug(f"Geohash encoding: {lat}, {lon}")
    geohash = geohash2.encode(lat, lon, precision=10)
    log.debug(f"Returned value: {geohash}")
    return pd.Series({"geohash": geohash})


async def import_excel_to_dataframe(
    input_data: Union[Path, BytesIO], char_encoding: str = None
) -> pd.DataFrame:
    """Import .xlsx to Pandas dataframe."""
    try:
        if char_encoding:
            return pd.read_excel(input_data, index_col=None, encoding=char_encoding)
        else:
            return pd.read_excel(input_data, index_col=None)
    except Exception as e:
        log.error("Excel data import failed. Is it in the correct format?")
        log.exception(e)
        raise Exception from e("Excel data import failed. Is it in the correct format?")


async def import_csv_to_dataframe(
    input_data: Union[Path, BytesIO], char_encoding: str = None
) -> pd.DataFrame:
    """Import .csv to Pandas dataframe."""
    try:
        if char_encoding:
            return pd.read_csv(input_data, index_col=None, encoding=char_encoding)
        else:
            return pd.read_csv(input_data, index_col=None)
    except Exception as e:
        log.error("CSV data import failed. Is it in the correct format?")
        log.exception(e)
        raise Exception from e("CSV data import failed. Is it in the correct format?")


def check_required_fields(df: pd.DataFrame, required_fields: list) -> NoReturn:
    """Check required fields are in a given dataframe."""
    df_fields = df.columns.values.tolist()
    if missing := list(set(required_fields) - set(df_fields)):
        log.error(f"Imported data has missing fields: {missing}")
        raise KeyError("Missing required fields. Check spelling and letter case.")
    else:
        log.info("All required fields present.")

    required_df = df[df.columns.intersection(required_fields)]
    null_rows = required_df[required_df.isnull().any(axis=1)]
    if not null_rows.empty:
        # Increment all indexes by one, to match excel
        null_rows.index += 1
        msg = f"Rows {null_rows.index.tolist()} contain empty data in required fields."
        log.error(msg)
        raise KeyError(msg)


def validate_spelling_against_nearest_db_value(
    df: pd.DataFrame, field_name: str, field_options: list
) -> pd.DataFrame:
    """Match dataframe values to closest value in database. Spelling corrector."""
    log.debug("Extracting values not present in DB field")
    incorrect_values_mask = (
        ~df[field_name].isin(field_options) & df[field_name].notnull()
    )

    if incorrect_values_mask is False:
        log.info(f"No mis-spelt values detected for field ({field_name})")
    else:
        log.info(f"Matching mis-spelt values for field ({field_name}) to DB values")
        df.loc[incorrect_values_mask, field_name] = df[field_name].apply(
            lambda x: get_close_matches(x, field_options, n=1)
        )

    log.debug(f"Exploding column {field_name} to convert lists to strings")
    df = df.explode(field_name)

    return df


@validate_data_schema(data_schema=SiteInPydantic)
def return_site_df(
    sites: pd.DataFrame, data_owner: str, required_fields: list
) -> pd.DataFrame:
    """Return and validate (mast) sites dataframe.

    df: pd.DataFrame
        Input dataframe extracted from excel.
    data_owner: str
        The id of the data from the user database table.
    required_fields: list
        List of required field names.
    """
    log.info("Processing input Site dataframe")
    log.debug(f"Provided dataframe for validation: {sites}")

    # Handle coordinates
    log.debug("Encoding WGS84 coordinates to Geohash (level 10).")
    sites = sites.join(
        sites.apply(lambda x: geohash_encode(x.WSG84_lat, x.WSG84_lon), axis="columns")
    )
    log.debug("Dropping WGS84 fields.")
    sites.drop(columns=["WSG84_lon", "WSG84_lat"], inplace=True)
    required_fields.remove("WSG84_lon")
    required_fields.remove("WSG84_lat")
    required_fields.append("geohash")

    # GENUS
    # Match mis-spelt genus
    sites = validate_spelling_against_nearest_db_value(sites, "genus", genus_options)
    # Genus no longer required, determined from species_id
    required_fields.remove("genus")

    # SPECIES
    # Setting value to None for missing species
    if "species" not in sites.columns:
        log.debug("Creating species column")
        sites["species"] = None
    log.debug("Writing <Genus> sp. to empty species entries")
    sites.loc[sites["species"].isnull(), "species"] = sites["genus"] + " sp."
    # Match mis-spelt species
    sites = validate_spelling_against_nearest_db_value(
        sites, "species", species_options.keys()
    )
    # Get species id for text values
    sites["species"] = sites["species"].map(species_options, na_action="ignore")
    # Rename species to species_id
    log.debug("Renaming species column to species_id")
    sites.rename(columns={"species": "species_id"}, inplace=True)
    # Add species_id to required fields
    required_fields.append("species_id")

    # MAST TYPE
    log.debug("Determining mast_type from mast_name")
    # Match mis-spelt mast name
    sites = validate_spelling_against_nearest_db_value(
        sites, "mast_name", mast_options.keys()
    )
    # Get mast id for text values
    sites["mast_name"] = sites["mast_name"].map(mast_options, na_action="ignore")
    # Rename mast_name to mast_type_id
    log.debug("Renaming mast_name column to mast_type_id")
    sites.rename(columns={"mast_name": "mast_type_id"}, inplace=True)
    # Replace mast_name with mast_type in required field
    required_fields.remove("mast_name")
    required_fields.append("mast_type_id")

    # DATE
    if "date_observation" in sites.columns:
        required_fields.append("date_observation")
        log.debug(
            "Generating datetimes for date_observation field in Europe/Zurich locale"
        )
        sites["date_observation"] = pd.to_datetime(
            sites["date_observation"],
            errors="coerce",
            dayfirst=True,
        ).dt.tz_localize("Europe/Zurich")
        log.debug("Setting NaN values to None")
        sites[["date_observation"]] = (
            sites[["date_observation"]]
            .astype(object)
            .where(sites[["date_observation"]].notnull(), None)
        )
        log.debug(f"date_observation data type: {sites['date_observation'].dtype}")
        log.debug(f"date_observation values {sites[['date_observation']].to_dict()}")

    # USER
    log.debug(f"Setting Site user id to {data_owner}")
    sites["user_id"] = data_owner
    required_fields.append("user_id")

    # Systematic
    if "systematic" in sites.columns:
        log.debug("'systematic' field identifed, checking if bool type")
        if sites["systematic"].dtypes == "bool":
            log.debug("Adding to required fields")
            required_fields.append("systematic")
        else:
            log.warning("'systematic' field is not type bool, omitting")

    # Comments
    if "comments" in sites.columns:
        log.debug("'comments' field identifed, adding to required fields")
        required_fields.append("comments")

    log.info(f"Extracting fields from dataframe: {required_fields}")
    sites = sites[required_fields]

    return sites


async def import_mast_data(
    input_data: Union[str, BytesIO, Path] = current_working_dir / "scripts/data",
    user_id: str = "auth0|62fb4830f76949850e9e6ef2",
    file_extension: str = ".xlsx",
    char_encoding: str = "utf-8",
) -> NoReturn:
    """Import an data file into database, via API call or direct module invoke.

    input_data: Union[str, BytesIO, Path]
        Input XLSX/CSV file. Either a string, pathlib Path, or BytesIO memory object.
        Directories can be specified and files will be globbed and interated.
        Defaults to scripts/data/ directory for test data.
    user_id: str
        ID of user from database, for the user_id field in Site.
    file_extension: str
        File extension determined from file upload (only via API).
    """
    log.debug(f"Input provided: {input_data}")

    if isinstance(input_data, BytesIO):
        log.info("Reading dataframe directly from memory")
        if file_extension == ".xlsx":
            df = await import_excel_to_dataframe(
                input_data, char_encoding=char_encoding
            )
        elif file_extension == ".csv":
            df = await import_csv_to_dataframe(input_data, char_encoding=char_encoding)
        else:
            log.error("Uploaded file must be in .xlsx or .csv format")
            raise FileNotFoundError("Invalid input provided for import")
    elif (path := Path(input_data)).is_dir():
        log.debug(f"Data directory found: {path}. Iterating files...")
        for file_path in path.glob("**/*.xlsx"):
            await import_mast_data(input_data=file_path, user_id=user_id)
        for file_path in path.glob("**/*.csv"):
            await import_mast_data(input_data=file_path, user_id=user_id)
    elif path.exists() and path.suffix == ".xlsx":
        log.info(f"Reading {path.name} into dataframe.")
        df = await import_excel_to_dataframe(path)
    elif path.exists() and path.suffix == ".csv":
        log.info(f"Reading {path.name} into dataframe.")
        df = await import_csv_to_dataframe(path)
    else:
        log.error("Value must be an xlsx file, dir, Pathlib, or BytesIO")
        raise FileNotFoundError("Invalid input provided for import")

    required_fields = [
        "WSG84_lon",
        "WSG84_lat",
        "genus",
        "mast_name",
        "mast_year",
        "observer",
    ]

    check_required_fields(df, required_fields)
    validated_df = return_site_df(df, user_id, required_fields)

    if __name__ == "__main__":
        # Only init db if outside FastAPI
        log.debug("Initialising database connection")
        await db_init(TORTOISE_ORM)

    log.debug("Extracting records from dataframe as list of dicts")
    site_list = validated_df.to_dict(orient="records")

    log.info("Inserting records into database")
    await Site.bulk_create([Site(**fields) for fields in site_list])


if __name__ == "__main__":
    run_async(import_mast_data())
