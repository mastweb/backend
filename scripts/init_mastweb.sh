#!/bin/bash

aerich init --tortoise-orm app.db.TORTOISE_ORM
aerich --app mastapi init-db
aerich --app mastapi migrate
aerich --app mastapi upgrade

echo "Init database with geo functions."
python -m scripts.db
