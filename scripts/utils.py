"""Utils module for MastAPI, for debugging and logging."""

import logging
import os
import sys
from pathlib import Path
from typing import NoReturn, Union

log = logging.getLogger(__name__)


def _debugger_is_active() -> bool:
    """Check to see if running in debug mode.

    Returns:
    -------
        bool: if a debug trace is present or not.
    """
    gettrace = getattr(sys, "gettrace", lambda: None)
    return gettrace() is not None


def _is_docker() -> bool:
    """Check to see if running in a docker container.

    Returns:
    -------
        bool: if a docker related components present on filesystem.
    """
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


def load_dotenv_if_in_debug_mode(env_file: Union[Path, str]) -> NoReturn:
    """Load secret .env variables from repo for debugging.

    Args:
        env_file (Union[Path, str]): String or Path like object pointer to
            secret dot env file to read.
    """
    try:
        from dotenv import load_dotenv
    except ImportError as e:
        log.error(
            """
            Unable to import dotenv.
            Note: The logger should be invoked after reading the dotenv file
            so that the debug level is by the environment.
            """
        )
        log.error(e)
        raise ImportError from e(
            """
            Unable to import dotenv, is python-dotenv installed?
            Try installing this package using pip install envidat[dotenv].
            """
        )

    is_docker_from_env = os.getenv("IS_DOCKER", default=False)
    if _debugger_is_active() and not is_docker_from_env:
        secret_env = Path(env_file)
        if not secret_env.is_file():
            log.error(
                """
                Attempted to import dotenv, but the file does not exist.
                Note: The logger should be invoked after reading the dotenv file
                so that the debug level is by the environment.
                """
            )
            raise FileNotFoundError(
                f"Attempted to import dotenv, but the file does not exist: {env_file}"
            )
        else:
            load_dotenv(secret_env)


def terminal_log(log_name: str, log_level="DEBUG") -> logging.Logger:
    """Create basic terminal logger."""
    logging.basicConfig(
        level=log_level,
        format=(
            "%(asctime)s.%(msecs)03d [%(levelname)s] "
            "%(name)s | %(funcName)s:%(lineno)d | %(message)s"
        ),
        datefmt="%Y-%m-%d %H:%M:%S",
        stream=sys.stdout,
    )
    return logging.getLogger(log_name)
