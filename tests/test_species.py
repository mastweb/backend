# mypy: no-disallow-untyped-decorators
# pylint: disable=E0611,E0401

"""Tests for Species endpoints."""

import asyncio
from typing import Generator

import pytest
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer

from app.main import app
from app.models.species import Species


@pytest.fixture(scope="module")
def client() -> Generator:
    """Init pytest client."""
    initializer(["models"])
    with TestClient(app) as c:
        yield c
    finalizer()


@pytest.fixture(scope="module")
def event_loop(client: TestClient) -> Generator:
    """Init pytest async event loop."""
    yield client.task.get_loop()


def test_create_species(
    client: TestClient, event_loop: asyncio.AbstractEventLoop
):  # nosec
    """Test species creation."""
    response = client.post(
        "/species",
        json={
            "scientific_name": "test",
            "genus": "test",
            "family": "test",
            "name_english": "test",
            "name_german": "prüfung",
            "name_french": "essai",
            "name_italian": "prova",
            "priority": "false",
        },
    )
    assert response.status_code == 200, response.text
    data = response.json()
    assert "id" in data
    assert data["scientific_name"] == "test"
    assert data["genus"] == "test"
    assert data["family"] == "test"
    assert data["name_english"] == "test"
    assert data["name_german"] == "prüfung"
    assert data["name_french"] == "essai"
    assert data["name_italian"] == "prova"
    assert data["priority"] is False

    species_id = data["id"]

    async def get_species_by_db():
        species = await Species.get(id=species_id)
        return species

    species_obj = event_loop.run_until_complete(get_species_by_db())
    assert species_obj.id == species_id
